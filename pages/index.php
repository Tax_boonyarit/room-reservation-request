<!doctype html>
<html lang="en">
<?php
include('../config/connectDB.inc.php');
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ระบบจองห้องพิเศษ</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/libs/css/style.css">
    <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="index.php"> <i class=" fas fa-hospital"></i> ระบบจองห้องพิเศษ</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">


                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/admin.png" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <a class="dropdown-item" href="#"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                MeNu
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="index.php"> <i class="fas fa-home"> หน้าหลัก</i> </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class=" fas fa-address-book"></i>จัดการผู้ใช้</a>
                                <div id="submenu-2" class="collapse submenu" style="">
                                    <ul class="nav flex-column">

                                        <li class="nav-item">
                                            <a class="nav-link" href="users.php">แสดงข้อมูลผู้ใช้</a>
                                        </li>

                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw fa-chart-pie"></i>จัดการห้องพิเศษ</a>
                                <div id="submenu-3" class="collapse submenu" style="">
                                    <ul class="nav flex-column">

                                        <li class="nav-item">
                                            <a class="nav-link" href="addRoom.php">เพิ่มห้องพิเศษ</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="room.php">แสดงห้องพิเศษ</a>
                                        </li>

                                    </ul>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="requestRoom.php"> <i class="fas fa-list-alt">
                                        คำร้องขอจองห้องพัก</i>
                                </a>
                            </li>



                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h2> <i class="fas fa-hospital-alt"></i> ห้องพิเศษทั้งหมด</h2>
                            </div>
                            <div class="card-body bg-primary text-white text-center">
                                <?php
                                $sqlCountRoom = "SELECT * FROM `room` ";
                                $resultAll = mysqli_query($dbcon, $sqlCountRoom);
                                $coutRoomAll = mysqli_num_rows($resultAll);
                                ?>
                                <p style="font-size: 50px"><?php echo $coutRoomAll; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h2> <i class="fas fa-calendar-plus"></i> จำนวนห้องที่ว่าง</h2>
                            </div>
                            <?php
                            $sqlCountRoomNull = "SELECT * FROM `room` WHERE `Status` = 'ว่าง'";
                            $resultNull = mysqli_query($dbcon, $sqlCountRoomNull);
                            $coutRoomNull = mysqli_num_rows($resultNull);
                            ?>
                            <div class="card-body bg-success text-white text-center">
                                <p style="font-size: 50px"><?php echo $coutRoomNull; ?></p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h2> <i class="fas fa-calendar-times"></i> จำนวนห้องที่ไม่ว่าง</h2>
                            </div>

                            <?php
                            $sqlCountRoomNotNull = "SELECT * FROM `room` WHERE `Status` = 'ไม่ว่าง'";
                            $resultNotNull = mysqli_query($dbcon, $sqlCountRoomNotNull);
                            $coutRoomNotNull = mysqli_num_rows($resultNotNull);
                            ?>
                            <div class="card-body bg-danger text-white text-center">
                                <p style="font-size: 50px"><?php echo $coutRoomNotNull; ?></p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h2> <i class="fas fa-people-carry"></i> ผู้ร้องขอจองห้องพิเศษ</h2>
                            </div>
                            <div class="card-body bg-primary text-white text-center">
                                <?php
                                $sqlCountReq = "SELECT * FROM `form` ";
                                $resultReq = mysqli_query($dbcon, $sqlCountReq);
                                $coutRoomReq = mysqli_num_rows($resultReq);
                                ?>
                                <p style="font-size: 50px"><?php echo $coutRoomReq; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h2> <i class=" far fa-check-square"></i> อนุมัติแล้ว</h2>
                            </div>
                            <?php
                            $sqlCountReqApp = "SELECT * FROM `form` WHERE `Status` = 'อนุมัติ'";
                            $resultReqApp = mysqli_query($dbcon, $sqlCountReqApp);
                            $coutRoomReqApp = mysqli_num_rows($resultReqApp);
                            ?>
                            <div class="card-body bg-success text-white text-center">
                                <p style="font-size: 50px"><?php echo $coutRoomReqApp; ?></p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h2> <i class="fas fa-hourglass-start"></i> รอการอนุมัติ</h2>
                            </div>

                            <?php
                            $sqlCountReqSup = "SELECT * FROM `form` WHERE `Status` = 'ivอนุมัติ'";
                            $resultReqSup = mysqli_query($dbcon, $sqlCountReqSup);
                            $coutRoomReqSup = mysqli_num_rows($resultReqSup);
                            ?>
                            <div class="card-body bg-warning text-white text-center">
                                <p style="font-size: 50px"><?php echo $coutRoomReqSup; ?></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            โรงพยาบาลอุตรดิตถ์ Uttaradit Hospital
                            เลขที่ 38 ถนนเจษฏาบดินทร์ ตำบลท่าอิฐ อำเภอเมือง จังหวัดอุตรดิตถ์ 53000

                            เบอร์โทรศัพท์ 0-5540-9999
                            <a href="http://www.uttaradit-hosp.go.th:81/utthosp/">WebSite</a>.
                        </div>

                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets/libs/js/main-js.js"></script>
</body>

</html>